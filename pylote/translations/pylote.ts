<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="696"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="183"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="223"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="191"/>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="199"/>
        <source>Next file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="264"/>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="272"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="703"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="89"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="391"/>
        <source>Lock instruments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="430"/>
        <source>Show false cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="289"/>
        <source>New screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="303"/>
        <source>White page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="310"/>
        <source>Dotted paper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="317"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="324"/>
        <source>Choose background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="333"/>
        <source>Ruler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="352"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="371"/>
        <source>Protractor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="381"/>
        <source>Compass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="704"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="450"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="457"/>
        <source>Curve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="464"/>
        <source>Highlighter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="471"/>
        <source>Add text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="478"/>
        <source>Add point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="485"/>
        <source>Add pixmap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="74"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="769"/>
        <source>Styles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="68"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="520"/>
        <source>Delete selected object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="527"/>
        <source>Erase all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_instruments.py" line="277"/>
        <source>Point name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="410"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="447"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="115"/>
        <source>&lt;P&gt;&lt;/P&gt;&lt;P ALIGN=LEFT&gt;Here you can select on which screen you will to work.&lt;/P&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="62"/>
        <source>Screen usage mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="66"/>
        <source>All space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="68"/>
        <source>Full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="96"/>
        <source>Screen number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="423"/>
        <source>Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="215"/>
        <source>Icon size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="315"/>
        <source>Enter a value between {0} and {1}:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="106"/>
        <source>Insert text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="615"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="236"/>
        <source>Visible actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="283"/>
        <source>Screenshot delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="285"/>
        <source>in milliseconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="431"/>
        <source>Tools window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="439"/>
        <source>Kids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="215"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="207"/>
        <source>Previous file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="250"/>
        <source>Export SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="506"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="513"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="279"/>
        <source>Create a launcher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="401"/>
        <source>Lock units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="409"/>
        <source>Save units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="423"/>
        <source>Reset units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="361"/>
        <source>Square (not graduated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="333"/>
        <source>Print configuration (and PDF export)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="339"/>
        <source>Print mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="341"/>
        <source>Full page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="343"/>
        <source>True size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="354"/>
        <source>Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="356"/>
        <source>Portrait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="358"/>
        <source>Landscape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="82"/>
        <source>&lt;p align=left&gt;&lt;b&gt;All space: &lt;/b&gt;the application use all the free space on desktop.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Full screen: &lt;/b&gt;choose this if you ave problems with All space mode.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=center&gt;&lt;b&gt;If you change this, you need to restart application.&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="378"/>
        <source>&lt;p align=left&gt;&lt;b&gt;Full page: &lt;/b&gt;printing will be adapted to the dimensions of the page.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;True size: &lt;/b&gt;the printed document comply with the dimensions of your figures.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="896"/>
        <source>Custom width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="308"/>
        <source>Attach distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="310"/>
        <source>between lines or points and ruler or square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="416"/>
        <source>Restore backed up units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="499"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="667"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="110"/>
        <source>About {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="70"/>
        <source>(version {0})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="565"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="572"/>
        <source>Dash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="786"/>
        <source>Select color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="842"/>
        <source>Select pen width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="579"/>
        <source>Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="586"/>
        <source>Dash Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="593"/>
        <source>Dash Dot Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="92"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="97"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="728"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="819"/>
        <source>Delete this item ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="988"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1252"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1300"/>
        <source>not a valid file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1322"/>
        <source>Failed to open
  {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="526"/>
        <source>Open Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="71"/>
        <source>Pylote Files (*.plt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="523"/>
        <source>Image Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1061"/>
        <source>Failed to save
  {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1543"/>
        <source>Export as SVG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1545"/>
        <source>SVG Files (*.svg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1600"/>
        <source>Export as PDF File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1602"/>
        <source>PDF Files (*.pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="697"/>
        <source>Geometry instruments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="759"/>
        <source>Basic tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="761"/>
        <source>Screenshots and backgrounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="763"/>
        <source>Drawing tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="870"/>
        <source>click to edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="765"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="872"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="767"/>
        <source>Widths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="891"/>
        <source>Custom color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="170"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="492"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1573"/>
        <source>Export as PNG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1575"/>
        <source>PNG Files (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="257"/>
        <source>Export PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="342"/>
        <source>Ruler (not graduated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="688"/>
        <source>Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1097"/>
        <source>No name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1110"/>
        <source>File must be saved ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="175"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="296"/>
        <source>Transparent area</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
